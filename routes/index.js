module.exports = function( app ) {
  var mongojs = require('mongojs'),
      db = mongojs('express-todo-list');

  require('./tasks')( app, db, mongojs );
};