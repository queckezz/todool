module.exports = function( app, db, mongojs ) {
  db.tasks = db.collection('tasks');

  app.get('/', function( req, res ) {
    var docs = {
      done: [],
      undone: []
    };

    db.tasks.find().sort({ $natural: -1 }, function( err, docsDB ) {
      for (var doc in docsDB ) {
        if ( docsDB[ doc ].done ) {
          docs.undone.push( docsDB[ doc ] );
        } else {
          docs.done.push( docsDB[ doc ] );
        }
        if ( +doc + 1 === docsDB.length ) {
          res.render('index', {
            done: docs.done,
            undone: docs.undone,
            stylesheets: [ '/stylesheets/main.css' ],
            javascripts: [ '/javascripts/lib/jquery-min.js', '/javascripts/todo.js' ]
          });
        }      
      }
    });      
  });

  app.post('/add', function( req, res ) {
    var timestamp = new Date()
    
    db.tasks.insert({
      task: req.body.task,
      done: false,
      created_at: timestamp
    }, { safe: true }, function( err, records ) {
      res.send( records[0]._id );
    });
  });

  app.post('/delete', function( req, res ) {
    var objectId,
        i = 0,
        length = req.body.data.length;

    console.log( 'data: ' + req.body.data );

    for (; i < length; i++ ) {
      objectId = mongojs.ObjectId( req.body.data[ i ] );
      
      db.tasks.remove({
        '_id': objectId
      }, function( err, saved ) {
        if( err || !saved ) {
          console.log("Problem removing this user :" + err);
        }
      });
    }
  });

  app.post('/complete', function( req, res ) {
    var objectId,
        i = 0,
        length = req.body.data.length;

    for (; i < length; i++ ) {
      objectId = mongojs.ObjectId( req.body.data[ i ] );

      db.tasks.update({
        '_id': objectId
      }, {
        $set: { 'done': true }
      }, function( err, saved ) {
        if( err || !saved ) {
          console.log("Problem removing this user :" + err);
        }     
      });
    } 
  });
};