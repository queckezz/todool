![Todool](http://dl.dropbox.com/u/81152350/logo.png)

## Installation
  Create the app:
  ```
  $ mkdir todool
  $ git clone https://github.com/queckezz/todool.git
  $ cd todool
  ```
  Start the server:
  ```
  $ node app
  ```
  Or with nodemon:
  ```
  $ npm start
  ```
## Built on
* [Mongojs](https://github.com/gett/mongojs)
* [Express](http://expressjs.com/)
* [Jade templating engine](http://jade-lang.com/)
* [Stylus preprocessor](http://learnboost.github.com/stylus/)

## Features
* Add Tasks
* Remove Tasks
* Move Tasks to a completed Tasks section

## Screenshots
  A screenshot of the app:

  ![screen_1](http://dl.dropbox.com/u/81152350/screen_1.png)