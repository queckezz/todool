$(document).ready(function() {
  (function( $, todool, window, undefined ) {
    var $todo_search = $('.todo-search'),
        $todo_toolbar_delete = $('.todo-toolbar-delete'),
        $todo_toolbar_complete = $('.todo-toolbar-complete'),

        $todo_list_checkboxes,
        $todo_list_checkboxes_checked;    

    var checkboxHandler = function() {
      $todo_list_checkboxes = $('input:checkbox');

      $todo_list_checkboxes.on('click', function() {
        $todo_list_checkboxes_checked = $('input:checkbox:checked');
        console.log( $todo_list_checkboxes_checked );
      });
    };

    notify = function( msg, type ) {
        
    }

    todool.run = function() {      
      checkboxHandler();

      $todo_search.keypress(function( event ) {
        if ( event.which == 13 ) {
          if ( $todo_search.val() !== '' ) {
              $.post('/add', { task: $todo_search.val() }, function( data ) {
                $('<li>' + $todo_search.val() + ' <input class="todo-list-checkbox" type="checkbox" data="' + data + '" /></li>').prependTo( $('.todo-list').eq( 0 ) ).hide().slideDown( 200 );
                
                $todo_search.val('');

                $todo_list_checkboxes.off('click');
                checkboxHandler();
              });

              return false;
            }
          }
      });

      $todo_toolbar_delete.on('click', function() {
        var dataAttr = [];

        $todo_list_checkboxes_checked.each(function( i ) {
          $self = $( this );
          
          dataAttr.push( $self.attr('data') );

          if ( $todo_list_checkboxes_checked !==0 ) {
            $.post('/delete', { data: dataAttr }, function( data ) {
              console.log('yo');
              $self.parent().slideUp(200, function() {
                $self.remove();
              });
            });
          }
        });
      });

      $todo_toolbar_complete.on('click', function() {
        var dataAttr = [];

        $todo_list_checkboxes_checked.each(function( i ) {
          $self = $( this );
          
          dataAttr.push( $self.attr('data') );

          if ( $todo_list_checkboxes_checked !==0 ) {
            $.post( '/complete', { data: dataAttr } );
          }
        });
      });
    };
  })( jQuery, window.todool || ( window.todool = {} ), window );

  todool.run();
});