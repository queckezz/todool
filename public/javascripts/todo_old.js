$(document).ready(function() {
  var appendQueueText = function() {
    $('.todo-toolbar-queue-text').text( checkbox.checked.length + ' of ' + $('input:checkbox').length + ' currently selected' );
  };

  var updateCheckBoxHandler = function(  ) {
    $('input:checkbox').click(function() {
      if ( $( this ).is(':checked') ) {
        checkbox.checked.push( $( this ) );
      } else {
        var index = $(this).index('input') - 1;
        checkbox.checked.splice( checkbox.checked.indexOf( checkbox.checked[ index ], 1 ) );
      }
      
      appendQueueText();
    });    
  }

  $('.todo-search').keypress(function ( e ) {
    var todoSearch = $('.todo-search');
    
    if ( e.which == 13 ) {
      var data = {
        task: todoSearch.val()
      };

      $.post('/add', data, function( resObj ) {
        $('<li>' + todoSearch.val() + ' <input class="todo-list-checkbox" type="checkbox" data="' + resObj + '" /></li>').prependTo( $('.todo-list').eq( 0 ) ).hide().slideDown( 200 );

        $('input:checkbox').off('click');
        updateCheckBoxHandler();

        todoSearch.val('');

        $('.msgbar-msg').text('Task successfully added!');
        $('.msgbar').removeClass('info').removeClass('error').addClass('success').fadeIn( 400 ).delay( 2000 ).fadeOut( 400 );

        appendQueueText();
      });

      return false;
    }
  });

  var checkbox = {
    checked: []
  };

  appendQueueText()
  updateCheckBoxHandler()

  var data = {
    checkboxes: []
  };

  $('.todo-toolbar-delete').click(function() {

    for ( var i = 0; i < checkbox.checked.length; i++ ) {
      data.checkboxes.push( checkbox.checked[ i ].attr('data') );

      checkbox.checked[ i ].parent().slideUp(200, function() {
        $( this ).remove();

        var index = $(this).index('input') - 1;
        checkbox.checked.splice( checkbox.checked.indexOf( checkbox.checked[ index ], 1 ) );
        appendQueueText();

        $('.msgbar-msg').text('Task successfully removed!');
        $('.msgbar').removeClass('info').removeClass('success').addClass('error').fadeIn( 400 ).delay( 2000 ).fadeOut( 400 );
      });
    }
    if ( data.checkboxes.length !== 0 ) {
      $.post( '/delete', data )
    }
  });

  $('.todo-toolbar-complete').click(function() {
    for ( var i = 0; i < checkbox.checked.length; i++ ) {
      data.checkboxes.push( checkbox.checked[ i ].attr('data') );

      checkbox.checked[ i ].parent().slideUp(200, function() {
        $( this ).prependTo( $('.second') ).hide().slideDown( 200 );
        $( this ).children().attr( 'checked', false );
        appendQueueText()

        $('.msgbar-msg').text('Task successfully moved and completed!');
        $('.msgbar').removeClass('error').removeClass('success').addClass('info').fadeIn( 400 ).delay( 2000 ).fadeOut( 400 );
      });
    }
    if ( data.checkboxes.length !== 0 ) {
      $.post( '/complete', data );
    }    
  });
});