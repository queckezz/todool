var express = require('express'),
    http = require('http'),
    stylus = require('stylus'),

    app = express();

app.configure(function() {
  app.set( 'port', process.env.PORT || 3000 );
  app.set( 'views', __dirname + '/views' );
  app.set( 'view engine', 'jade' );

  // =middleware conf
  app.use( express.bodyParser() );
  app.use( express.methodOverride() );
  app.use( app.router );

  app.use(stylus.middleware({
    src: __dirname + '/views',
    dest: __dirname + '/public',
  }));
  app.use( express.static( __dirname + '/public' ) );
});

require('./routes')( app )

http.createServer( app ).listen(app.get('port'), function() {
  console.log( 'App running on port: ' + app.get('port') );
});